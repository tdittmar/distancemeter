byte pingPin = 6;
byte inPin = 7;
byte redLedPin = 13;
byte greenLedPin = 12;

// Each bit of flags represents one bool.
// 76543210
// ||||||||
// |||||||+- 0 = Green LED OFF, 1 = Green LED ON
// ||||||+-- 0 = Green LED SOLID, 1 = Green LED BLINK
// |||||+--- 0 = Red LED OFF, 1 = Red LED ON
// ||||+---- 0 = Red LED SOLID, 1 = Red LED BLINK
// |||+-----
// ||+------
// |+-------
// +--------
byte flags = 0;

void setup() 
{
  Serial.begin(9600);

  // Configure pins
  pinMode(redLedPin, OUTPUT);
  pinMode(greenLedPin, OUTPUT);
  pinMode(pingPin, OUTPUT);
  pinMode(inPin, INPUT);

  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
}
 
void loop()
{
  // establish variables for duration of the ping,
  // and the distance result in inches and centimeters:
  int duration, cm;

  // 10us ping on the ping pin 
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
 
  // Read the HIGH time of the in pin
  duration = pulseIn(inPin, HIGH, 500000);
  
  // A constant wait. The time for "no obstacle" is 38 milliseconds.
  // We make sure measurement takes exactly 38 ms.
  if (duration > 0)
    delayMicroseconds(500000 - duration);
 
  // convert the time into a distance
  cm = microsecondsToCentimeters(duration);
  
  // Is there an object closer than 2 meters? Show green light
  // Is there an object closer than 50 cm? Show red light
  // Is there an object closed than 20 cm? Blink red light
  if (cm <= 200 && cm > 50)
    flags = B00000001;
  else if (cm <= 50 && cm > 20)
    flags = B00000100;
  else if (cm <= 20 && cm > 0)
  {
    flags = flags & B00000100; // Turn off all green features
    flags = flags | B00001000; // Red should blink
    flags = flags ^ B00000100; // Turn LED on or off
    Serial.println(flags, BIN);
  }
  else
  {
    flags = 0;
  }
  

  setLED();
  
  delay(300);
}


void setLED()
{
  // Red LED should be ON?
  if (flags & B00000100)
    digitalWrite(redLedPin, HIGH);
  else
    digitalWrite(redLedPin, LOW);  

  // Green LED should be ON?
  if (flags & B00000001)
    digitalWrite(greenLedPin, HIGH);
  else
    digitalWrite(greenLedPin, LOW);  
}
 
 
long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

